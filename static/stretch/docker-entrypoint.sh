#!/usr/bin/env bash

if [ "$NGINX_ENV" = "prod" ]; then
    echo "Nothing specific for prod to do here so far..."
fi

exec "$@"
