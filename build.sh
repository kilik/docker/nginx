#!/bin/bash

source .include.sh

PLATFORM="linux/amd64,linux/arm64"

function display_help
{
  echo -e ""
  echo -e "Script to build docker images"
  echo -e ""
  echo -e "${COLOR_GREEN}build.sh${COLOR_DEFAULT} --path ${COLOR_BLUE}<path>${COLOR_DEFAULT} --registry ${COLOR_BLUE}<registry>${COLOR_DEFAULT} --version ${COLOR_BLUE}<version>${COLOR_DEFAULT} [--pull] [--push] [--help]"
  echo -e "    --path        ex: symfony/4.x/buster"
  echo -e "    --registry    ex: kilik/nginx"
  echo -e "    --version     ex: symfony-4.x-buster"
  echo -e "    --pull        pull image before build"
  echo -e "    --push        push image after build"
  echo -e "    --platform    ex: linux/amd64,linux/arm64"
  echo -e ""
}

# parse command line
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -r|--path)
    DOCKER_PATH="$2"
    shift # past argument
    shift # past value
    ;;
    -r|--registry)
    REGISTRY="$2"
    shift # past argument
    shift # past value
    ;;
    -v|--version)
    VERSION="$2"
    shift # past argument
    shift # past value
    ;;
    --platform)
    PLATFORM="$2"
    shift # past argument
    shift # past value
    ;;
    --timestamp)
    WITH_TIMESTAMP=YES
    shift # past argument
    ;;
    --pull)
    PULL="--pull"
    shift # past argument
    ;;
    --push)
    PUSH="--push"
    shift # past argument
    ;;
    --help)
    display_help
    exit 1
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

# @param $1 argument name
# @param $2 value
function checkArgument
{
  if [ -z "$2" ]; then
    echo -e "${COLOR_RED}missing argument ${COLOR_BLUE}$1${COLOR_DEFAULT}"

    display_help
    exit 1
  fi
}

checkArgument "version" "$VERSION"
checkArgument "path" "$DOCKER_PATH"
checkArgument "registry" "$REGISTRY"

TIMESTAMP=`date "+%y.%W"`

echo -e "VERSION    : ${COLOR_BLUE}${VERSION}${COLOR_DEFAULT}"
echo -e "DOCKER_PATH: ${COLOR_BLUE}${DOCKER_PATH}${COLOR_DEFAULT}"
echo -e "REGISTRY   : ${COLOR_BLUE}${REGISTRY}${COLOR_DEFAULT}"
echo -e "TIMESTAMP  : ${COLOR_BLUE}${TIMESTAMP}${COLOR_DEFAULT}"

REGISTRY_PATH="${REGISTRY}:${VERSION}"
REGISTRY_PATH_TIMESTAMP="${REGISTRY}:${VERSION}-${TIMESTAMP}"

echo -e "${COLOR_YELLOW}building image ${COLOR_BLUE}${REGISTRY_PATH}${COLOR_DEFAULT}"
docker buildx build --platform ${PLATFORM} -t ${REGISTRY_PATH} ${PULL} ${DOCKER_PATH} ${PUSH}

if [ "YES" == "$WITH_TIMESTAMP" ]; then
  echo -e "${COLOR_YELLOW}tagging timestamped image ${COLOR_BLUE}${REGISTRY_PATH_TIMESTAMP}${COLOR_DEFAULT}"
  docker buildx build --platform ${PLATFORM} -t ${REGISTRY_PATH_TIMESTAMP} ${PULL} ${DOCKER_PATH} ${PUSH}
fi
