# Web server Nginx (docker version)

## Work on this project

```shell
cd ~/docker
git clone git@gitlab.com:kilik/docker/nginx.git
cd nginx
```

## Build images locally

Append options to publish images on registry.
```shell
OPTS=--push
```

```shell
./build.sh --help
./build.sh --path symfony/4.x/buster --registry kilik/nginx --version symfony-4.x-buster --pull --timestamp ${OPTS}
./build.sh --path symfony/4.x/bullseye --registry kilik/nginx --version symfony-4.x-bullseye --pull --timestamp ${OPTS}
./build.sh --path symfony/4.x/bullseye --registry kilik/nginx --version symfony-4.x --pull --timestamp ${OPTS}
./build.sh --path legacy/stretch --registry kilik/nginx --version legacy-stretch --pull --timestamp ${OPTS}
./build.sh --path legacy/bullseye --registry kilik/nginx --version legacy-bullseye --pull --timestamp ${OPTS}
./build.sh --path legacy/bullseye --registry kilik/nginx --version legacy --pull --timestamp ${OPTS}
```

# CI - Continuous integration

This project don't use CI anymore (problems with shared runners).

Maintained tags:

* symfony-4.x, symfony-4.x-bullseye
* symfony-4.x-buster
* legacy, legacy-bullseye
* legacy-stretch

# Use this image in your projects

## Understand volumes in image

* /var/www/html/web is your website root
* /var/www/html is your project root

docker-compose.yml sample:

```yml
services:
    nginx:
        image: kilik/nginx:symfony-4.x-stretch
```
