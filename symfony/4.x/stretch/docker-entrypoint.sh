#!/usr/bin/env bash

if [ "$NGINX_ENV" = "prod" ]; then
    echo "Nothing specific for prod to do here so far..."
fi

SYNFONY_HOST_FILE="/etc/nginx/sites-enabled/symfony.conf"

exec "$@"

